## Remote intra documentation

# Installation

Ensure you can access the remote location with the following command:

```bash
    ssh consumer-driven
```

To achieve this, you need to add something like the following block to ~/.ssh/config

```bash
Host consumer-driven
  Hostname ec2-52-59-216-2.eu-central-1.compute.amazonaws.com
  Port 22
  User ec2-user
  IdentityFile ~/.ssh/2018-10-aws.pem
```

# Usage

There is a taskscript available to make it a bit easier to update the remote location and setting up the tunnels.

This command shows you what is available:
```bash
    ./task.sh help
```

Update the remote location to represent the new docker config:

```bash
    ./task.sh updateRemote
```

Stop the remote

```bash
    ./task.sh stopRemote
```

Setup the tunnels

```bash
    ./task.sh startTunnel
```

Stop the tunnels

```bash
    ./task.sh stopTunnel
```