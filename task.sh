#!/bin/bash

ProgName=$(basename $0)

# Prepare ec2 instance tips:
# yum install docker
# https://docs.docker.com/compose/install/#install-compose
# https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user


MACHINE=consumer-driven

function task_help(){
    task_commands
}

function task_commands(){       # Get help on the possible commands
	 grep "^function task_" $0 | sed 's/function task_/ /g; s/(){ #/:         /g; s/(){/ /g'
}

function task_startLocal(){     # Start the metric catching environment locally
	docker-compose up -d && docker-compose logs -f
}

function task_stopLocal(){      # Stop the metric catching environment locally
	docker-compose down
}

function task_updateRemote(){   # Update the remote docker services
	rsync -avz --exclude-from=rsyncexclusions . $MACHINE:~/ci-pact-infra/

    ssh $MACHINE <<-'ENDSSH'
cd ~/ci-pact-infra/
docker-compose up -d
ENDSSH

}

function task_stopRemote(){     # Stop the remote application

    ssh $MACHINE <<-'ENDSSH'
cd ~/ci-pact-infra/
docker-compose down
ENDSSH

}


function task_startTunnel(){    # Start the tunnels
    if [ -e "tunnel-socket" ]; then
        echo "Tunnel is already up"
        return
    fi
    
    echo "Starting the tunnel"
    ssh -M -S tunnel-socket -fNL 9081:localhost:8081 -L 9082:localhost:8082 -L 49160:localhost:49160 -L 1111:localhost:1111 -L 2222:localhost:2222 $MACHINE
}

function task_stopTunnel(){     # Stop the tunnels

    if [ ! -e "tunnel-socket" ]; then
        echo "No socket file available. Check if something is still running with ps aux | grep ssh and kill manually."
        return
    fi

    echo "Stopping the tunnel"
    # kill trick from:     # https://stackoverflow.com/questions/2241063/bash-script-to-setup-a-temporary-ssh-tunnel/15198031#15198031
   	ssh -S tunnel-socket -O exit $MACHINE
}

subcommand=$1
case $subcommand in
    "" | "-h" | "--help")
        task_help
        ;;
    *)
        shift
        echo "Running '${subcommand}' command."
        task_${subcommand} $@
        if [ $? = 127 ]; then
            echo "Error: '$subcommand' is not a known subcommand." >&2
            echo "       Run '$ProgName --help' for a list of known subcommands." >&2
            exit 1
        fi
        ;;
esac