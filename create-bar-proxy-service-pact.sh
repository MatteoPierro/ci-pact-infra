#!/bin/sh

PACT_BROKER=${PACT_BROKER:-"http://localhost:9082"}

curl -v -X PUT "${PACT_BROKER}/pacts/provider/Sign%20up%20Service/consumer/Bar%20proxy%20Service/versions/1.0.0" -H "Content-Type: application/json" --data '
{
    "provider": {
        "name": "Sign up Service"
    },
    "consumer": {
        "name": "Bar proxy Service"
    },
    "interactions": []
}'
