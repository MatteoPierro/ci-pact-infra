#!/bin/sh

PACT_BROKER=${PACT_BROKER:-"http://localhost:9082"}

curl -v -X PUT "${PACT_BROKER}/pacts/provider/Sign%20up%20Service/consumer/Location%20Promotion%20Service/versions/latest" -H "Content-Type: application/json" --data '
{
    "provider": {
        "name": "Sign up Service"
    },
    "consumer": {
        "name": "Location Promotion Service"
    },
    "interactions": []
}'
