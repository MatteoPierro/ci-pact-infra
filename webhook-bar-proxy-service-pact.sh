#!/bin/sh

PACT_BROKER=${PACT_BROKER:-"http://localhost:9082"}

curl -v -X POST "${PACT_BROKER}/webhooks" -H "Content-Type: application/json" --data '
{
  "provider": {
    "name": "Sign up Service"
  },
  "consumer": {
    "name": "Bar proxy Service"
  },
  "events": [{
    "name": "contract_content_changed"
  }],
  "request": {
    "method": "POST",
    "url": "http://jenkins:8080/jenkins/buildByToken/build?job=sign-up-service&token=pact"
  }
}
'

curl -v -X POST "${PACT_BROKER}/webhooks" -H "Content-Type: application/json" --data '
{
  "provider": {
    "name": "Sign up Service"
  },
  "consumer": {
    "name": "Bar proxy Service"
  },
  "events": [{
    "name": "provider_verification_published"
  }],
  "request": {
    "method": "POST",
    "url": "http://jenkins:8080/jenkins/buildByToken/build?job=deploy-bar-proxy&token=pact"
  }
}
'
