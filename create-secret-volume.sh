#!/bin/sh

docker volume create mail-secret || echo "volume is already created"
docker run -v mail-secret:/secret --name helper busybox true
docker cp ./secret/. helper:/secret
docker rm helper