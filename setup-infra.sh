#!/bin/sh

sh ./create-secret-volume.sh
docker network create microservices-net || echo "network already available"
docker-compose up -d