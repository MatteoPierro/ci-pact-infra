# Pact-Jenkins infrastructure

## Requirements

- Docker
- Docker compose

## Setup

```bash
$ docker-compose up -d
```

### Services

- [Jenkins](http://localhost:8081/jenkins)
- [Pact Broker](http://localhost:8082)

## Jenkins

### Requirements

#### Plugins

- [Build Token Root](https://wiki.jenkins-ci.org/display/JENKINS/Build+Token+Root+Plugin)

- [Node Tool](https://medium.com/@gustavo.guss/jenkins-starting-with-pipeline-doing-a-node-js-test-72c6057b67d4)

### Setup envarionment variables

`Jenkins` &rarr; `Manage Jenkins` &rarr; `Configure System`
* Enable `Environment variables`
* Add variable `PACT_BROKER` with value `http://pact-broker`


## Pact Broker actions

### Create an empty Pact

```bash
$ curl -X PUT "http://localhost:8082/pacts/provider/Bar/consumer/Foo/versions/latest" -H "Content-Type: application/json" --data '
{
  "provider": {
    "name": "Bar"
  },
  "consumer": {
    "name": "Foo"
  },
  "interactions": []
}
'
```

### Create a webhook

```bash
$ curl -X POST "http://localhost:8082/webhooks" -H "Content-Type: application/json" --data '
{
  "provider": {
    "name": "Bar"
  },
  "consumer": {
    "name": "Foo"
  },
  "events": [{
    "name": "contract_content_changed"
  }],
  "request": {
    "method": "POST",
    "url": "http://master.ci.my.domain:8085/rest/api/latest/queue/SOME-PROJECT?os_authType=basic",
    "username": "username",
    "password": "password",
    "headers": {
      "Accept": "application/json"
    }
  }
}
'
```

### Delete a webhook

```bash
$ curl -X DELETE "http://localhost:8082/webhooks/<webhook_id>"
```

### Delete a participant

```bash
$ curl -X DELETE "http://localhost:8082/pacticipants/Bar"
```