---
theme : "solarized"
transition: "none"
highlightTheme: "darkula"
---

# Consumer Driven Contracts

### The Evolution of Your API Supported by Tests

<small>[Nelis](mailto:nelis@boucke.be) / [@nelisboucke](http://twitter.com/nelisboucke)<br>[Matteo](mailto:mpierro@wemanity.com) / [@matteo_pierro](http://twitter.com/@matteo_pierro)</small>

<img data-src="img/wemanity-web-retina-logo.png" alt="Wemanity logo" style="background:none; border:none; box-shadow:none;">

---

## A typicall situation

<img height="300" data-src="img/teams-for-api.png" alt="Teams involved in an API" style="background:none; border:none; box-shadow:none;">

Characteristics:

* Teams in a company need to integrate an API
* New functionality

Note: 
Team building api for payments. Complex business rules.

External? team building frontends to perform payments. One in iOS, a web one, an Android one. 

---

<img width="100" data-src="img/question.png" alt="Question" style="background:none; border:none; box-shadow:none;">

### How does the cooperation go?

---

## Provider driven

<img height="250" data-src="img/provider-driven.png" alt="Provider driven" style="background:none; border:none; box-shadow:none;">

* Provider builds the API as they see fit
* Exposes all information available
* Consumer starts when the API is ready

---

### Good for

* Expose API outside
* Lots of consumers
* Unknown consumers

Examples: google, stripe, meetup

---

### Challenges in our situation

<img height="250" data-src="img/bike-vs-rocket.png" alt="Teams involved in an API" style="background:none; border:none; box-shadow:none;">

* Ping-pong is needed to make it work in practice
* Lots of bugs, complaints, waiting, interupts
* Late feedback from end-user
* Often complex to achieve simple flows

Note:

* Often there is not contract!

---

## Contract driven

<img height="250" data-src="img/contract-driven.png" alt="Provider driven" style="background:none; border:none; box-shadow:none;">

* Agree on API in advance
* Build in parallel, use mock or stub
* Integrate in the end

Note:

* Swagger

---

### Good for

* Good when you know what you need
* Clearly describes what to expect
* Working with an external party you know

---

### Challenges in our situation

<img height="250" data-src="img/bridge-mismatch.png" alt="Teams involved in an API" style="background:none; border:none; box-shadow:none;">

* Analysis paralysis. Lots of meetings.
* API must evolve to allow integration
* Integration near deadline

Note:

* Long and boring meeting for details, lots of analysis needed.

---

## Consumer driven

<img height="250" data-src="img/consumer-driven.png" alt="Provider driven" style="background:none; border:none; box-shadow:none;">

* (Short) meeting to understand the needs and basic design
* Consumer iteratively builds + shares it expectation
* Provider iteratively builds using these expectations

---

### God for

* You do not know what you need
* Limited and known consumers
* Facing integration issues and questions faster (less rework, less deadline pressure)
* Continues feedback loop (integration, end-users)

---

### Challenges

* Tools needed to make this possible
  * Pact, Spring Cloud Contracts
* Provider needs to adapt WoW to encorporate consumer changes
* Consumer needs to work test driven and share expectations

---

## Pact

Consumer drive contract test tool

---

### Demo with bar service